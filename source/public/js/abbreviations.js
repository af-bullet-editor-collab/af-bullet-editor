try {
	var myText = $("#bullet");
	var bulletNew = myText.val().trim();

	// automatically replaces words with abbreviations
	var sGArray,
	sg;
	if(dropSG.val() == "als"){
		console.log(bulletNew);
		sGArray = Object.keys(sgALS);
		sg = sgALS;

		// sets the last study guide to ALS for unabbreviation purposes later
		lastSG = sgALS;
	} else if (dropSG.val() == "480W"){
		sGArray = Object.keys(sg480ISRW);
		sg = sg480ISRW;

		// sets the last study guide to 480th ISRW for unabbreviation purposes later
		lastSG = sg480ISRW;

	} else {
		throw new Error('Select style guide.');
	}

	// loops through all words w/ abbreviations in the style guide
	// loops in reverse order to prevent unintended replacements of short words
	sGArray.reverse().forEach(function(word) {
		var indices = getIndicesOf(word, bulletNew, false);
		if(indices !== false){
			//console.log(indices);

			// goes through each word that will be abbreviated and records its index
			// does this in reverse order to prevent errors in the indices caused by abbreviation
			indices.reverse().forEach(function(index){
				
				// adds indices to abbrevs
				abbrevs.push({
					index: index,
					word: word,
					abbrev: sg[word]
				});
				abbrevs.forEach(function(abbreviation){
					
					// checks if other indices will be thrown off by this abbreviation
					if(abbreviation.index > index){

						// fix potential conflicts by subtracting the difference between the original word and the abbreviation
						abbreviation.index = abbreviation.index - (word.length - sg[word].length);
					}
				});
			});
			
			if(isVerb(word + 'd') && word.split('').reverse().join().indexOf('e') === 0 && sg[word].split('').reverse().join().indexOf('e') !== 0) {
				console.log(word + 'd');
				specAbbrevs.push(sg[word]);
			}
		}
		
		bulletNew = bulletNew.replace(new RegExp('\\b(?:' + word + ')', 'gi'), sg[word]);
		bulletNew = bulletNew.replace(new RegExp('\\b(?:' + (word + 's') + ')', 'gi'), sg[word] + 's');
	});
	lastLength = bulletNew.length;
	console.log(abbrevs);
	myText.val(bulletNew);
}
catch(error){
	$('#error').text(error.message);
	$('#error').css('color', 'red');
}