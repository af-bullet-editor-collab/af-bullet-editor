// waits to start running script until the page has fully rendered
$(document).ready(function(){
	var bullets,
	styleGuide;

	// makes bullet textbox editable
	$('#bullets').editable({

		// makes textbox support the use of multiple lines
		multiline: true,

		// makes output update every half second
		saveDelay: 500,

		/*
		* updates output when user is done making an edit
		* called "save" because it was initially designed by the creators with saving to a database in mind
		*/
		save: function(e, ui) {

			// splits the text into individual bullets by newline and puts them into a bullet array
			var bullets = ui.content.split(/\r?\n/);

			// if 480ISRW style guide is selected
			if($('#sg').val() == '480ISRW') {

				// overwrite style guide, as defined in "sg-480ISRW.js"
				styleGuide = sg480ISRW;
			}
			else if($('#sg').val() == 'als') {
				styleGuide = sgALS;
			}

			// loops through each bullet in the array
			bullets.forEach(function(bullet, index, bullets) {
				console.log(`${index}: ${bullet}`);

				/* 
				* gets all of the keys for the style guide as an array in reverse order
				* this is done in reverse order to prevent words that are within other words from messing up the abbreviation process
				* Ex: if 'assist' and 'assistance' both abbreviate to 'asst', in regular order, 'assistant' would end up as 'asstant'
				*/
				var words = Object.keys(styleGuide).reverse();

				// loops through each word in the current style guide
				words.forEach(function(word) {

					// replaces each occurance of the current word with its abbreviation within the current bullet
					bullet = bullet.replace(new RegExp(word, 'gi'), '<span class="abbrev">' + styleGuide[word] + '</span>');
				});

				// wraps current bullet in HTML for styling
				bullets[index] = '<div class="bullet">' + bullet + '</div>';
			});

			// re-writes the output to contain the updated bullet HTML elements by conjoining the bullet array
			$('#output').html(bullets.join(''));
		}
	});
});