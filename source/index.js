const compression = require('compression')
var bodyParser = require('body-parser');
const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
//import permit from "./permission"; // middleware for checking if user's role is permitted to make request

//Init App
var app = express();
const server = app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
var routes = require('./routes/index');
app.use(compression())
app
  // support json encoded bodies
  .use(bodyParser.json())
  // support encoded bodies
  .use(bodyParser.urlencoded({ extended: true })) 
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs');

app.use('/', routes);
