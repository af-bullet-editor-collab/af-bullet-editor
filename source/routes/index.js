var express = require('express');
var router = express.Router();

/**
 * Gets the homepage
 */
router.get('/', function (req, res) {
    res.render('pages/index');
});

module.exports = router;
