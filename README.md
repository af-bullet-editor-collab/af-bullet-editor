# EPR/OPR/Award Bullet Analyzer

An application designed to save our Airmen and our entire force time

## Quick Start
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs #This project requires NodeJS
npm install;             #Installs Dependencies
npm start;               #Launches Express server at localhost:5000
```

## Summary 


## File Structure
```
.
├── app.json                               #Node package information
├── conf.json
├── CONTRIBUTING.md                        #List of contributors
├── package.json
├── package-lock.json
├── Procfile
├── README.md
└── source                                 #Contains all source code and html files that are served
    ├── index.js                           #This index.js file launches the express.js webserver, serving files to users
    ├── package-lock.json  
    ├── public                             #Anything in the public folder is can be view by anyone.
    │   ├── css        
    │   ├── images
    │   ├── js
    ├── routes
    │   └── index.js                      #This index.js contains all top-level routing information. What is provided to ajax calls, ect.
    └── views                             #Holds all templated html
        ├── pages
        └── partials

```

